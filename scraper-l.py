import scrapy

class BrickSetSpider(scrapy.Spider):
    name = "brickset_spider"
    start_urls = ['https://www.letras.mus.br/legiao-urbana/#']

    def parse(self, response):
        SET_SELECTOR = 'ul.cnt-list li'
        for song in response.css(SET_SELECTOR)[:]:

            NAME_SELECTOR = 'a ::text'
            NEXT_PAGE_SELECTOR = 'a ::attr(href)'
            # yield {
            #     'name': song.css(NAME_SELECTOR).extract_first()
            #
            # }

            next_page = song.css(NEXT_PAGE_SELECTOR).extract_first()
            if next_page:
                yield scrapy.Request(
                    response.urljoin(next_page),
                    callback=self.print
                )

    def print(self, response):
        LYRIC_SELECTOR = '.cnt-letra p::text'
        TITLE_SELECTOR = '.cnt-head_title h1 ::text'
        # yield {
        #     'title': response.css(TITLE_SELECTOR).extract_first(),
        #     'lyric': response.css(LYRIC_SELECTOR).getall()
        # }
        #print(response.css(TITLE_SELECTOR).extract_first())
        filename = 'letras.txt'
        file = open(filename, 'a')
        file.write(response.css(TITLE_SELECTOR).extract_first())
        file.write('\n')
        file.write('\n'.join(response.css(LYRIC_SELECTOR).getall()))
        file.write('\n')
        file.write('\n')

